// Styles
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";

// Vuetify
import { createVuetify } from "vuetify";

export default createVuetify({
	theme: {
		themes: {
			light: {
				dark: false,
				colors: {
					primary: "#a8cc3b", // Ganti dengan warna yang Anda inginkan
					secondary: "#2d3285",
					accent: "#393945",
					error: "#FF5252",
					info: "#2196F3",
					success: "#4CAF50",
					warning: "#FFC107",
				},
			},
		},
	},
});
