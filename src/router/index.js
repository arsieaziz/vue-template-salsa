import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import AboutView from "../views/AboutView.vue";
import OurMenuView from "../views/OurMenuView.vue";
import TextFieldView from "../views/TextFieldView.vue";
import Login from "@/components/Login.vue";
import Dashboard from "@/components/Dashboard.vue";

const routes = [
	{
		path: "/",
		name: "Login",
		component: Login,
	},
	{
		path: "/dashboard",
		name: "Dashboard",
		component: Dashboard,
		children: [
			{ path: "", redirect: "dashboard/home" },
			{
				path: "home",
				component: HomeView,
				meta: {
					title: "Home",
				},
			},
			{
				path: "about",
				component: AboutView,
				meta: {
					title: "About",
				},
			},
			{
				path: "our-menu",
				component: OurMenuView,
				meta: {
					title: "Our Menu",
				},
			},
			{
				path: "text-field",
				component: TextFieldView,
				meta: {
					title: "Text Field",
				},
			},
		],
	},
];

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
});

export default router;
